[![CircleCI](https://circleci.com/gh/abhidp/Qloyal.svg?style=svg)](https://circleci.com/gh/abhidp/workflows/Qloyal/tree/feature%2FAbhi-QloyalCodeTest)


This repo contains Tests for both API and Web ACs.

* __API Test framework details__ : 
    * [Axios](https://github.com/axios/axios) HTTP Client
    * [Mocha](https://mochajs.org/) Test Runner
    * [Chai](https://www.chaijs.com/) Assertion Library
    * [Mochawesome](https://www.npmjs.com/package/mochawesome) Reporter
    * Testscases - 

          AC1 - Get Weather by Lattitude & Longitude
          AC2 - Get Weather by PostCode

* __WEB Test Framework details__ :
    * [WebdriverIO](https://webdriver.io/) Selenium based Framework
    * [Mocha](https://mochajs.org/) Test Runner
    * [Chai](https://www.chaijs.com/) Assertion Library
    * [Wdio Timeline Reporter](https://www.npmjs.com/package/wdio-timeline-reporter) (with screenshots of each step)
    * Testscases - 

          Login Test
          AC1 - Book Featured Tours as per Users input


### Instructions :

#### Pre-Reqs :

* Node
* Npm
* JDK
* Chrome
* Firefox

#### Get Started :
* One-liner clone and install -
```
git clone https://github.com/abhidp/Qloyal.git && 
cd Qloyal && 
npm install
```

* Create a `.env` file at the root of this repo and populate the values taking `.env_sample` as a reference 
  
* Run API Tests -  
    * `npm run apitest`

    * View API Test Report - open `api-test-report.html` present under `test-results/api/` folder

    * Testcase summary command line snapshot - 

        <img src=img/API_result.png width=500>

* Run WEB Tests - 
    * `npm run webtest`

    *  View WEB Test Report - open `0-web-timeline-report.html` present under `test-results/web/` folder
    
    * Testcase summary command line snapshot - 

       <img src=img/WEB_result.png width=800>




## CI/CD Architecture:

Complete e2e CI/CD is accomplished using : 

* [CircleCi](https://circleci.com/gh/abhidp/workflows/Qloyal/tree/feature%2FAbhi-QloyalCodeTest)
* [DockerHub](https://hub.docker.com/)
* [Google Cloud Platform](https://cloud.google.com/)
* [Kubernetes](https://kubernetes.io/)
* [Zalenium](https://opensource.zalando.com/zalenium/)


<img src=img/CiCdArch.jpg width=900>



1) When user pushes code to GitHub, CircleCi workflow is triggered and executes the following workflow: 

    <img src=img/circleciWorkflow.png width=350>
    
    * Step 1 -  creates a Docker Image of the Test Code and Pushes to DockerHub
    * Step 2 - parallel execution of a. and b.
    
        * a. executes API tests inside a docker container using the image created in Step 1 and generates Mochawesome test report and stores under Artifacts 
        * b. executes WEB tests inside a docker container using the image created in Step 1 and generates Wdio-Timeline test report and stores under Artifacts

2) Execution of WEB tests in the pipeline needs a lot of additional setup and dependencies which is described as below:   

* [Zalenium](https://opensource.zalando.com/zalenium/) is a Containerized Auto-scalable Selenium Grid with support for Chrome and Firefox, was Deployed on a Kubernetes Cluster hosted in Google Cloud Platform.
After tests are finished these browser pods are automatically shut down.
    <img src="img/ZaleniumDeployement.png" width=800>

* Selenium Grid was exposed as a LoadBalancer which can be accessed publicly by WebdriverIO tests. Based on capabilities from the Test code, Selenium Grid then spins up instances of Chrome and Firefox inside separate containers and runs tests. 
    <img src="img/ZaleniumService.png" width=800>

      

    
    **Selenium Grid** is Live on Kubernetes GCP @
    http://35.197.175.36/grid/console

    **Live Preview** of Test Executions
    http://35.197.175.36/grid/admin/live

    **Video Recodings** of Test Executions(with embeded Sub-Titles/TestSteps)
    http://35.197.175.36/dashboard/#


### Bonus: Run the CI Pipeline yourself and live preview browsers running tests in the cloud -

1)  Head over to https://circleci.com/gh/abhidp/workflows/Qloyal/tree/feature%2FAbhi-QloyalCodeTest

2) Rerun the latest Workflow

3) Check the status of the Workflow and wait till Web Tests are triggered

3) Once Web Tests Job has started, goto the Live preview link mentioned above

4) If you missed the live preview, dont worry, you can view their recordings anytime by going to the Dashboard link mentioned above

5) Once the Workflow is finished, you can view the test reports from within CircleCi
  
  * API - Navigate to the individual Job of API Test --> 
  Click Artifacts -->  root/project/test-results/api/api-test-report.html
  * WEB - Navigate to the individual Job of WEB Test --> 
  Click Artifacts -->  root/project/test-results/web/0-web-timeline-report.html

  OR

Instead of triggering the build from CircleCI, you can run the tests from your local machine against the Cloud hosted Dockerized Selenium Grid

That is, instead of Steps 1) and 2) just do a `npm run webtest-ci` from your local and then follow Step 3) onwards










