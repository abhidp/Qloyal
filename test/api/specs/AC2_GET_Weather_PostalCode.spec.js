import axios from 'axios'
import { expect } from 'chai'
import * as config from '../config/apiConfig'
import * as expected from '../config/testData.json'

describe('/GET Current Weather Data by Postal Code', async () => {
    var response
    before(async () => {
        const path = '/current?'
        const query = config.getPostalCode()
        const getRequest = {
            method: 'GET',
            url: config.requestUrl(path, query),
            header: config.requestHeader()
        }
        response = await axios(getRequest)
    })

    it('Should return Successful response Object with status 200-OK', async () => {
        expect(response)
            .to.be.an('object')
            .to.include({
                status: 200,
                statusText: 'OK'
            })
    })

    it('Should contain valid keys in the response Body', async () => {
        expect(response.data.data[0])
            .to.include.all.keys(config.GETresponseBodyKeys)
    })

    it('Should return the correct City based on the Postal code', () => {
        expect(response.data.data[0].city_name)
            .to.equal(expected.postal_code.city)
    })
})