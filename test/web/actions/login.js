import LoginPage from '../pageObjects/loginPage'
import AccountPage from '../pageObjects/accountPage'

var email, password

export function openLoginPage() {
    LoginPage.open()
    dismissCookieAlert()
}

export function dismissCookieAlert() {
    LoginPage.dismissCookie.click()
}

const credentials = {
    valid: {
        email: process.env.EMAIL,
        password: process.env.PASSWORD
    },
    invalid: {
        email: 'incorrect@email.com',
        password: 'wrongpassword'
    }
}

export function login(params) {
    if (params !== undefined && (params.toLowerCase()).includes('invalid')) {
        email = credentials.invalid.email
        password = credentials.invalid.password
    } else {
        email = credentials.valid.email
        password = credentials.valid.password
    }

    LoginPage.inputEmail.setValue(email)
    LoginPage.inputPassword.setValue(password)
    LoginPage.loginButton.click()
}

export function loginErrorMsg() {
    return LoginPage.loginErrorMsg.getText()
}

export function loginSuccessful() {
    AccountPage.welcomeMessage.waitForDisplayed()
    return AccountPage.welcomeMessage.isExisting()
}