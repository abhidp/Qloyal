import BookingsPage from '../pageObjects/bookingsPage'

export function bookingSummaryVisible() {
    return BookingsPage.bookingSummary.isExisting()
}

export function confirmBooking() {
    BookingsPage.confirmBookingButton.click()
}

export function invoiceGenerated() {
    BookingsPage.invoiceSection.waitForDisplayed()
    return BookingsPage.invoiceSection.isExisting()
}

export function selectPayOnArrival() {
    BookingsPage.payOnArrivalButton.waitForDisplayed()
    BookingsPage.payOnArrivalButton.click()
}

export function paymentStatus(status) {

    browser.waitUntil(() => {
        return (BookingsPage.paymentStatus.getText()).includes(status)
    }, 10000, 'Payment Status Unchanged')
    return BookingsPage.paymentStatus.getText()
}