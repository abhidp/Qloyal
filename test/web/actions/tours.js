import ToursPage from '../pageObjects/toursPage'

export function featuredToursListed() {
    return ToursPage.featuredTours.length >= 1
}

export function selectFeaturedTour(tours) {
    const tourlist = ToursPage.featuredTours
    const tourname = ToursPage.featuredTourNames
    for (let i = 0; i < tourlist.length; i++) {
        if (((tourname[i]).getText().toLowerCase()).includes(`${tours}`.toLowerCase())) {
            tourlist[i].click()
            break
        }
    }
}

export function clickBookNow() {
    ToursPage.bookNowButton.click()
}

