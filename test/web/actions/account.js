import AccountPage from '../pageObjects/accountPage'

export function navigateToHotelsPage() {
    AccountPage.hotelsLink.click()
}

export function navigateToToursPage() {
    AccountPage.toursLink.click()
}