export function landingPageTitle() {
    return browser.getTitle()
}

export function landingPageURL() {
    return browser.getUrl()
}

export function acceptConfirmation() {
    browser.waitUntil(() => {
        return browser.getAlertText()
    }, 10000, 'Alert Not Present')
    browser.acceptAlert()
}

export function cancelConfirmation() {
    browser.waitUntil(() => {
        return browser.getAlertText()
    }, 10000, 'Alert Not Present')
    browser.dismissAlert()
}