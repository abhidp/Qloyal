import Page from './page'

export default new class LoginPage extends Page {
    get inputEmail() { return $(`//input[@name="username"]`) }
    get inputPassword() { return $(`//input[@name="password"]`) }
    get loginButton() { return $(`//button[@type="submit"]`) }
    get loginErrorMsg() { return $(`//div[@class="alert alert-danger"]`) }
    get dismissCookie() { return $(`//button[@id="cookyGotItBtn"]`) }

    open() {
        super.open('login');
    }
}