import Page from './page'

export default new class BookingsPage extends Page {
    get bookingSummary() { return $(`//*[contains(text(),'Booking Summary')]`) }
    get confirmBookingButton() { return $(`//button[@class="btn btn-success btn-lg btn-block completebook"]`) }
    get invoiceSection() { return $(`//div[contains(text(),'Invoice')]`) }
    get paymentStatus() { return $(`(//table/tbody/tr/td/div)[1]`) }
    get payOnArrivalButton() { return $(`//button[@class="btn btn-default arrivalpay"]`) }
}