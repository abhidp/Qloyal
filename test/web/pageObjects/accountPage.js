import Page from './page'

export default new class AccountPage extends Page {
    get welcomeMessage() { return $(`//div[@class="col-md-6 go-right RTL"]`) }
    get flightsLink() { return $(`//li[@data-title="flights"]`) }
    get hotelsLink() { return $(`//li[@data-title="hotels"]`) }
    get toursLink() { return $(`//li[@data-title="tours"]`) }
    get carsLink() { return $(`//li[@data-title="cars"]`) }
    get confirmBookingButton() { return $(`//button[contains(text(),'CONFIRM')]`) }

    open() {
        super.open('account')
    }
}