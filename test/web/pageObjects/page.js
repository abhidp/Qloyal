export default class Page {
    open(path) {
        browser.url(path)
        browser.setTimeout(
            { 'implicit': 30000 },
            { 'pageLoad': 30000 }
        )
        browser.setWindowSize(1280, 960)
    }

}
