import Page from './page'

export default new class ToursPage extends Page {
    get searchHotelInput() { return $(`id="select2-drop"`) }
    get featuredTours() { return $$(`//div[@class="hotel-image"]`) }
    get featuredTourNames() { return $$(`//div[@class="hotel-body"]`) }
    get bookNowButton() { return $(`//button[@class="btn btn-block btn-action btn-lg loader"]`) }
    get bookingSummary() { return $(`//*[contains(text(),'Booking Summary')]`) }
    get confirmBookingButton() { return $(`//button[contains(text(),'CONFIRM THIS BOOKING')]`) }
    get invoiceSection() { return $(`//div[contains(text(),'Invoice')]`) }

    open() {
        super.open('m-tours')
    }
}
