import {
    openLoginPage,
    login,
    loginErrorMsg,
    loginSuccessful
} from '../actions/login'
import { landingPageTitle, landingPageURL } from '../util/common'

describe('Login Tests', () => {
    it('Open PHPTravels Login page', () => {
        openLoginPage()
        expect(landingPageTitle()).to.equalIgnoreCase('Login')
        expect(landingPageURL()).to.contain('login')
    })

    it('Should throw error if Login credentials are Invalid', () => {
        login('Invalid Credentials')
        expect(loginErrorMsg()).to.equal('Invalid Email or Password')
    })

    it('Should redirect to My Account with Valid Login credentials', () => {
        login('Valid Credentials')
        expect(loginSuccessful()).to.equal(true)
        expect(landingPageTitle()).to.equalIgnoreCase('My Account')
        expect(landingPageURL()).to.contain('account')
    })
})