import { landingPageTitle, acceptConfirmation } from '../util/common'
import { navigateToToursPage } from '../actions/account'
import { openLoginPage, login, loginSuccessful } from '../actions/login'
import { selectFeaturedTour, featuredToursListed, clickBookNow } from '../actions/tours'
import { bookingSummaryVisible, confirmBooking, invoiceGenerated, paymentStatus, selectPayOnArrival } from '../actions/bookings'

/*
    Enter your preferred choice of Feature Tour booking (case insensitive)
    Accepted values are NILE, Alexandria, THAILAND, Bangkok, Hong Kong, DELHI, SYDNEY, BONDI, MALAYSIA, OMAN etc
*/
const myFeatureTourChoice = 'Sydney'

describe('Book a Featured Tour', () => {
    it('Login and Verify re-direction to My Account page', () => {
        openLoginPage()
        login()
        expect(loginSuccessful()).to.equal(true)
        expect(landingPageTitle()).to.include('My Account')
    })

    it('Navigate to Tours page and Verify Featured Tours are Listed', () => {
        navigateToToursPage()
        expect(featuredToursListed()).to.equal(true)
    })

    it('Select a Featured Tour as per User\'s choice', () => {
        selectFeaturedTour(myFeatureTourChoice)
        expect(landingPageTitle()).to.containIgnoreCase(myFeatureTourChoice)
    })

    it('Book Now and Verify Booking Summary', () => {
        clickBookNow()
        expect(bookingSummaryVisible()).to.equal(true)
    })

    it('Confirm Booking for the selected Tour', () => {
        confirmBooking()
        expect(invoiceGenerated()).to.equal(true)
        expect(paymentStatus('UNPAID')).to.equal('UNPAID')
    })

    it('Select Pay On Arrival and confirm Reservation Status', () => {
        selectPayOnArrival()
        acceptConfirmation()
        expect(paymentStatus('RESERVED')).to.include('RESERVED')
    })
})